<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class IndexController
{
    /**
     * @Route(name="index", path="/")
     */
    public function index() {
        return new JsonResponse([
            'wish' => 'keep at it',
            'result' => 'wish'
        ]);
    }
}