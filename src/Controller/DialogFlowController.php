<?php

namespace App\Controller;

use Exception;
use Google\ApiCore\ValidationException;
use Google\Cloud\Dialogflow\V2\Intent\Message;
use Google\Cloud\Dialogflow\V2\QueryInput;
use Google\Cloud\Dialogflow\V2\SessionsClient;
use Google\Cloud\Dialogflow\V2\TextInput;
use Google\Cloud\Dialogflow\V2\WebhookResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DialogFlowController extends AbstractController
{

    /**
     * @Route(name="getClientQuery", path="/messenger/call/{query}", options={"expose" = true})
     * @throws ValidationException
     * @throws Exception
     */
    public function getQuery(string $query): JsonResponse
    {
        $sessionClient = new SessionsClient(
            [
                'credentials' => realpath($this->getParameter('dialog_flow_config'))
            ]
        );

        $session = $sessionClient->sessionName($this->getParameter('dialog_flow_project'), bin2hex(random_bytes(32)));
        $textInput = new TextInput();
        $textInput->setText($query);
        $textInput->setLanguageCode('en-EN');

        $queryInput = new QueryInput();
        $queryInput->setText($textInput);
        //$queryParamsClass->knowledgeBaseNames='projects/'.$this->getParameter('dialog_flow_project').'/knowledgeBases/MTA4MDM4NDE5MzY0NTcyMDM3MTI';


        $response = $sessionClient->detectIntent($session, $queryInput);
        $queryResult = $response->getQueryResult();
        $fulfillmentText = $queryResult->getFulfillmentText();
        return new JsonResponse(array("response" => $fulfillmentText));
    }

    /**
     * @Route(name="dialogflow.webhook.receiver", path="/callback", methods={"POST"}, options={"expose" = true})
     */
    public function webhookReceiver(Request $request)
    {
        $response = new WebhookResponse();
        $response->setFulfillmentText('now now, play nice');
        $text = (new Message\Text())->setText(['simt ca mor']);
        $message = (new Message())->setText($text);
        $response->setFulfillmentMessages([$message]);
        $response->setSource('webhook');
        $jsonResponse = new Response(json_encode($response), 200);
        $jsonResponse->headers->set('Content-Type', 'application/json');
        return $jsonResponse;
    }
}